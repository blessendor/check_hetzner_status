# check_hetzner_status

Icinga2 / nagios check Hetzner Status RSS feed for interesting keyword in incident titles.

```
 ./check_hetzner_status.py -h
usage: check_hetzner_status.py [-h] -k INCIDENT_KEYWORD [-e EXCLUDE_KEYWORD_LIST] [-l LIMIT_RESOLVED] [-u RSS_FEED_URL]

Check Hetzner Status RSS feed for interested keyword in incident titles.

optional arguments:
  -h, --help            show this help message and exit
  -k INCIDENT_KEYWORD   Required. Input a keyword to finding relevant incidents. You can type a full phrase in "quotes" also.
  -e EXCLUDE_KEYWORD_LIST
                        Optional. Define a comma separated list of keywords to ignore any incidents with any keyword from the list.
  -l LIMIT_RESOLVED     Optional. Limit of founded resolved incidents to verbose info output (default: 3). If was founded more incidents - only summary info of their count
                        will be printed.
  -u RSS_FEED_URL       Optional. RSS feed URL. Script developed specially for Hetzner Status RSS feed, but it is possible to re-define https://status.hetzner.com/en.atom
                        (default), if it was changed by Hetzner.
```


**Example results**:

```
./check_hetzner_status.py -k dc2
OK: not found any listed incidents for [ dc2 ]

```
```
./check_hetzner_status.py -k 'storage box'
OK: all [ 18 ] listed incidents for [ storage box ] was resolved

```
```
./check_hetzner_status.py -k dc8

OK: all [ 2 ] listed incidents for [ dc8 ] was resolved

Title: Maintenance on router fsn1-dc8-ex9k1
Updated: 2021-03-03T03:37:30+00:00
https://status.hetzner.com/incident/24c988c7-78d2-4984-8bc4-4c60081fe968

Title: Maintenance on router fsn1-dc8-ex9k2
Updated: 2021-03-04T03:14:33+00:00
https://status.hetzner.com/incident/b26424d1-fb64-4a3b-ae13-bc357e51c2d8
```

```
./check_hetzner_status.py -k cloud
CRITICAL: found [ 1 ] not resolved incidents for [ cloud ]

Title: Fault report cloud node 15799
Start: 2021-03-09T14:54:51+00:00
Updated: 2021-03-09T14:54:51+00:00
https://status.hetzner.com/incident/ec4f349b-1260-4478-a76b-af5d01ac0e69
```

```

./check_hetzner_status.py -k dc1
OK: all [ 10 ] listed incidents for [ dc1 ] was resolved
```
```

./check_hetzner_status.py -k dc1-
OK: all [ 1 ] listed incidents for [ dc1- ] was resolved

Title: Maintenance on router fsn1-dc1-ex9k2
Updated: 2021-03-03T02:55:39+00:00
https://status.hetzner.com/incident/42bcd8a7-688c-4048-a49e-aed11ed963c3
```

```

 ./check_hetzner_status.py -k router
OK: all [ 19 ] listed incidents for [ router ] was resolved
```

```

./check_hetzner_status.py -k router -e ex9k2
OK: all [ 6 ] listed incidents for [ router ] was resolved
```

```

./check_hetzner_status.py -k router -e ex9k2 -l 7
OK: all [ 6 ] listed incidents for [ router ] was resolved

Title: Maintenance on router fsn1-dc6-ex9k1
Updated: 2021-03-03T02:55:21+00:00
https://status.hetzner.com/incident/8a03034d-f143-45ed-9b8c-f9475c803d8c

Title: Maintenance on router fsn1-dc7-ex9k1
Updated: 2021-03-03T03:32:01+00:00
https://status.hetzner.com/incident/3464a744-70fa-431a-a963-fb9cfbcc6ec9

Title: Maintenance on router fsn1-dc8-ex9k1
Updated: 2021-03-03T03:37:30+00:00
https://status.hetzner.com/incident/24c988c7-78d2-4984-8bc4-4c60081fe968

Title: Maintenance on router fsn1-dc13-ex9k1
Updated: 2021-03-03T03:43:51+00:00
https://status.hetzner.com/incident/d13b8308-89e8-4394-84e3-ffb8c39663ac

Title: Maintenance on router fsn1-dc16-ex9k1
Updated: 2021-03-03T03:53:23+00:00
https://status.hetzner.com/incident/0ebdf390-96bb-4221-a43e-db53356c66fe

Title: Maintenance on router fsn1-dc15-ex9k1
Updated: 2021-03-03T04:22:13+00:00
https://status.hetzner.com/incident/ddfa4242-049f-4046-81cf-52fc7855fcf7
```

```
./check_hetzner_status.py maintenance
OK: all [ 40 ] listed incidents for [ maintenance ] was resolved
```
#!/usr/bin/env python3
# General help for ElementTree - https://pep8.ru/doc/dive-into-python-3/14.html

# Oleksandr Usov <blessendor@gmail.com>
# Telegram @blessnedor
# 2021-03-06 v0.1 (one keyword argument version)
# 2021-03-09 v0.5 (make more flexible with much arguments)
# 2021-03-12 v0.9 (add supporting a list of exclude words, instead the only one word for exclude)


import argparse
import urllib.request
import re

parser = argparse.ArgumentParser(description='Check Hetzner Status RSS feed for interested keyword in incident titles.')

parser.add_argument('-k', type=str, required=True, dest='incident_keyword', help='Required. Input a keyword to finding relevant\n\
                    incidents. You can type a full phrase in "quotes" also.')
#parser.add_argument('-e', type=lambda s: [str(item) for item in s.split(',')], dest='exclude_keyword_list')
#Variant with creating item list later (after action of replace whitespaces before/after comma):
parser.add_argument('-e', type=str, dest='exclude_keyword_list',\
                    help='Optional. Define a comma separated list of keywords to ignore any incidents with any keyword from the list.')
parser.add_argument('-l', type=int, default=3, dest='limit_resolved', help='Optional. Limit of founded resolved incidents\n\
                    to verbose info output (default: 3). If was founded more incidents - only summary info of their count will be printed.')
parser.add_argument('-u', type=str, dest='rss_feed_url', default='https://status.hetzner.com/en.atom', \
                    help='Optional. RSS feed URL. Script developed specially for Hetzner Status RSS feed, but it is possible\
                    to re-define https://status.hetzner.com/en.atom (default),\
                    if it was changed by Hetzner.')

args = parser.parse_args()

incident_keyword = args.incident_keyword
limit_resolved = args.limit_resolved
rss_feed_url = args.rss_feed_url
exclude_keyword_list = args.exclude_keyword_list

try:
    # Define exclude words list and replacing whitespaces by comma
    # exclude_keyword_list = re.sub(r"\s+",",",str(args.exclude_keyword_list))
    # Keep whitespaces for accepting phrases, but replace whitespaces before/after comma, end remove whitespace from start/end
    if exclude_keyword_list is not None:
        exclude_keyword_list = re.sub(r"\s+,",",",str(re.sub(r",\s+",",",str(args.exclude_keyword_list)))).strip()
        #Finally, converting class 'str' to 'list'
        exclude_keyword_list = [str(item) for item in exclude_keyword_list.split(',')]

        # Debug of correct  converting string arguments to a list:
        # print(type(exclude_keyword_list))
        # print(len(exclude_keyword_list))
        # print('Ignored keywords:',exclude_keyword_list,'\n',incident_keyword)

except:
    print('Error /dev/hands')

# Check if provide URL/path is OK to open - exit with CRITICAL state and error msg

try:
    urllib.request.urlopen(rss_feed_url)
except urllib.error.HTTPError as h_err:
    # HTTPError exceptoion - when error in /../path
    err_msg = 'CRITICAL - can not open URL ../path/:\n\n{}\n\nURL: {}'
    print(err_msg.format(h_err, rss_feed_url))
    exit(2)  # exit code '2' - returns Critical for icinga / Nagios service state
except urllib.error.URLError as u_err:
    # URLError exceptoion - when problem with hostname resolving/opening
    err_msg = 'CRITICAL - can not resolve URL hostname:\n\n{}\n\nURL: {}'
    print(err_msg.format(u_err.reason, rss_feed_url))
    exit(2)  # exit code '2' - returns Critical for icinga / Nagios service state
else:
    with urllib.request.urlopen(rss_feed_url) as url:
        import xml.etree.ElementTree as etree

        tree = etree.parse(url)

        # a list for collection of interested incident titles
        lookup_title_list = []

        # a lists of resolved incidents
        resolved_incident_title_list = []
        resolved_incident_start_list = []
        resolved_incident_updated_list = []
        resolved_incident_link_list = []

        # a lists of not resolved incidents
        not_resolved_incident_title_list = []
        not_resolved_incident_start_list = []
        not_resolved_incident_updated_list = []
        not_resolved_incident_link_list = []

        entries = tree.findall('{http://www.w3.org/2005/Atom}entry')

        # Collect all interested titles
        for title in entries:
            # Get all titles
            title_element = title.find('{http://www.w3.org/2005/Atom}title')
            # print('title_element',title_element.text)

            # Lookup needed incidents with 'incident_keyword' and without any words from 'exclude_keyword_list'
            if re.search(incident_keyword, title_element.text, re.IGNORECASE):
                found_keywords_for_exclude = []
                if exclude_keyword_list is not None:
                    for item in range(len(exclude_keyword_list)):
                        if re.search(exclude_keyword_list[item], title_element.text, re.IGNORECASE):
                            # Collect found keywords to exclude current title from further operations
                            found_keywords_for_exclude.append(exclude_keyword_list[item])
                if not found_keywords_for_exclude:
                    # Add found incident into a list (for compare len() later)
                    lookup_title_list.append(title_element.text)
                    entry_link = title.find('{http://www.w3.org/2005/Atom}link')
                    entry_updated = title.find('{http://www.w3.org/2005/Atom}updated')
                    entry_content = title.find('{http://www.w3.org/2005/Atom}content')

                    for content_child in entry_content:
                        # a list of items from incident content
                        # (defined here bcs a list must be re-defined for each loop,
                        # otherwise it will collect content items from all entries)
                        entry_content_list = []
                        for div_child in content_child:
                            entry_content_list.append(div_child.text)
                    # Collect Resolved incidents titles
                    # if 'Resolved' in entry_content_list[3]: # Some Hetzner incident updated as resolved without Resolved tag
                    if 'Resolved' in str(entry_content_list):
                        resolved_incident_title_list.append(title_element.text)
                        resolved_incident_start_list.append(entry_content_list[0])
                        resolved_incident_updated_list.append(entry_updated.text)
                        resolved_incident_link_list.append(entry_link.attrib['href'])
                    # Else collect not resolved items
                    else:
                        not_resolved_incident_title_list.append(title_element.text)
                        not_resolved_incident_start_list.append(entry_content_list[0])
                        not_resolved_incident_updated_list.append(entry_updated.text)
                        not_resolved_incident_link_list.append(entry_link.attrib['href'])

# When not found any incident (will OK - output in last else)
if len(lookup_title_list) != 0:
    if len(resolved_incident_title_list) == len(lookup_title_list):
        print('OK: all [',len(resolved_incident_title_list),'] listed incidents for [',incident_keyword,'] was resolved\n')
        # Print detailed list of resolved incidents, if their total count <= 3 (or other default value of '-r' argument)
        # Useful when we interesting to lookup by some 'wildcard' type of keyword (i.e. 'router' or 'cloud')
        if len(resolved_incident_title_list) <= limit_resolved:
            for incident in range(len(resolved_incident_title_list)):
                print('Title:',resolved_incident_title_list[incident])
                # print((resolved_incident_start_list[incident].replace('T',' ')).replace('+00:00',' UTC')) #  Start:
                print('Updated:',(resolved_incident_updated_list[incident].replace('T',' ')).replace('+00:00',' UTC'))
                print(resolved_incident_link_list[incident])
                print()
                if exclude_keyword_list is not None:
                    print("Ignored:", exclude_keyword_list)
        exit(0) # # exit code '0' - returns OK for icinga / Nagios service state
    else:
        print('CRITICAL: found [',len(not_resolved_incident_title_list),'] not resolved incidents for [',incident_keyword,']\n')
        for incident in range(len(not_resolved_incident_title_list)):
            print('Title:',not_resolved_incident_title_list[incident])
            print((not_resolved_incident_start_list[incident].replace('T',' ')).replace('+00:00',' UTC'))
            print('Updated:',(not_resolved_incident_updated_list[incident].replace('T',' ')).replace('+00:00',' UTC'))
            print(not_resolved_incident_link_list[incident])
            print()
            if exclude_keyword_list is not None:
                print("Ignored:", exclude_keyword_list)
        exit(2)  # exit code '2' - returns Critical for icinga / Nagios service state

else:
    print('OK: not found any listed incidents for [',incident_keyword,']')
    if exclude_keyword_list is not None:
        print("Ignored:", exclude_keyword_list)
    exit(0) # exit code '0' - returns OK for icinga / Nagios service state
